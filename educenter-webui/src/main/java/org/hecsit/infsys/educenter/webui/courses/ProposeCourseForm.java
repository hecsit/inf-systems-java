package org.hecsit.infsys.educenter.webui.courses;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class ProposeCourseForm {

    @NotNull
    private Long departmentId;

    @NotEmpty
    @Size(max = 1024)
    private String initiator;

    @NotEmpty
    @Size(max = 1024)
    private String title;

    @NotEmpty
    @Size(max = 4000)
    private String motivation;

    public Long getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Long departmentId) {
        this.departmentId = departmentId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMotivation() {
        return motivation;
    }

    public void setMotivation(String motivation) {
        this.motivation = motivation;
    }

    public String getInitiator() {
        return initiator;
    }

    public void setInitiator(String initiator) {
        this.initiator = initiator;
    }
}
