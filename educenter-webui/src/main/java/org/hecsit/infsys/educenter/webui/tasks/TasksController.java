package org.hecsit.infsys.educenter.webui.tasks;

import org.activiti.engine.form.TaskFormData;
import org.activiti.engine.task.Task;
import org.hecsit.infsys.educenter.core.orgstructure.Department;
import org.hecsit.infsys.educenter.core.orgstructure.OrgStructureApi;
import org.hecsit.infsys.educenter.core.tasks.TaskType;
import org.hecsit.infsys.educenter.core.tasks.TasksApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("tasks")
public class TasksController {

    private final TasksApi tasksApi;
    private final OrgStructureApi orgStructureApi;

    @Autowired
    public TasksController(TasksApi tasksApi, OrgStructureApi orgStructureApi) {
        this.tasksApi = tasksApi;
        this.orgStructureApi = orgStructureApi;
    }

    @RequestMapping(value = "/by/{assignee}", method = RequestMethod.GET)
    public String listByAssignee(@PathVariable("assignee") String assignee, ModelMap model) {
        List<Task> tasks = tasksApi.getTasksByCandidateGroup(assignee);
        Department department = orgStructureApi.getDepartmentByCode(assignee);
        model.addAttribute("taskList", tasks);
        model.addAttribute("departmentCode", assignee);
        model.addAttribute("departmentTitle", department.getTitle());
        return "tasks/list";
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public String view(
            @PathVariable("id") String taskId,
            ModelMap model) {
        Task task = tasksApi.getTask(taskId);
        TaskFormData taskForm = tasksApi.getTaskForm(taskId);
        String assignee = tasksApi.getTaskCandidateGroup(taskId);
        Department department = orgStructureApi.getDepartmentByCode(assignee);
        model.addAttribute("task", task);
        model.addAttribute("taskForm", taskForm);
        model.addAttribute("departmentCode", assignee);
        model.addAttribute("departmentTitle", department.getTitle());

        if (TaskType.COURSE_DRAFT.is(task)
                || TaskType.COURSE_REVISION.is(task)
                || TaskType.COURSE_RECONCILIATION.is(task)
                || TaskType.COURSE_CONFIRMATION.is(task)
                || TaskType.COURSE_REGISTRATION.is(task)) {
            return "tasks/viewCourse";
        }
        return "tasks/viewGeneric";
    }

    @RequestMapping(value = "/{id}/complete", method = RequestMethod.POST)
    public String complete(@PathVariable("id") String taskId,
                           @RequestParam  Map<String, String> form,
                           RedirectAttributes redirectAttributes) {

        String assignee = tasksApi.getTaskCandidateGroup(taskId);
        tasksApi.complete(taskId, form);

        redirectAttributes.addFlashAttribute("message", "Task was completed.");
        return "redirect:/tasks/by/" + assignee;
    }
}
