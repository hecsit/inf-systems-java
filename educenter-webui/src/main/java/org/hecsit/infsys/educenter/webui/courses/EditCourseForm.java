package org.hecsit.infsys.educenter.webui.courses;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class EditCourseForm {

    @NotNull
    private Long id;

    @NotEmpty
    @Size(max = 1024)
    private String title;

    @NotNull
    private Long complexityId;

    @NotEmpty
    @Size(max = 4000)
    private String summary;

    @Size(max = 4000)
    private String comment;

    @Min(value = 0)
    @NotNull
    private Integer lectures;

    @Min(value = 0)
    @NotNull
    private Integer practice;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getComplexityId() {
        return complexityId;
    }

    public void setComplexityId(Long complexityId) {
        this.complexityId = complexityId;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Integer getLectures() {
        return lectures;
    }

    public void setLectures(Integer lectures) {
        this.lectures = lectures;
    }

    public Integer getPractice() {
        return practice;
    }

    public void setPractice(Integer practice) {
        this.practice = practice;
    }
}
