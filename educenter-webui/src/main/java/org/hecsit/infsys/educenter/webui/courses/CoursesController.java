package org.hecsit.infsys.educenter.webui.courses;

import org.activiti.engine.task.Task;
import org.hecsit.infsys.educenter.core.courses.Course;
import org.hecsit.infsys.educenter.core.courses.CourseComplexity;
import org.hecsit.infsys.educenter.core.courses.CourseNote;
import org.hecsit.infsys.educenter.core.courses.CoursesApi;
import org.hecsit.infsys.educenter.core.orgstructure.Department;
import org.hecsit.infsys.educenter.core.orgstructure.OrgStructureApi;
import org.hecsit.infsys.educenter.core.tasks.TasksApi;
import org.hecsit.infsys.educenter.core.workflow.WorkflowException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.text.MessageFormat;
import java.util.List;

@Controller
@RequestMapping("courses")
public class CoursesController {

    private final CoursesApi coursesApi;
    private final TasksApi tasksApi;
    private final OrgStructureApi orgStructureApi;

    @Autowired
    public CoursesController(CoursesApi coursesApi, TasksApi tasksApi, OrgStructureApi orgStructureApi) {
        this.coursesApi = coursesApi;
        this.tasksApi = tasksApi;
        this.orgStructureApi = orgStructureApi;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String list(ModelMap model) {
        List<Course> courses = coursesApi.getCourses();
        model.addAttribute(courses);
        return "courses/list";
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public String view(@PathVariable("id") Long id, ModelMap model) {
        Course course = coursesApi.getCourse(id);
        List<CourseNote> notes = coursesApi.getCourseNotes(id);
        List<Task> tasks = tasksApi.getTasksByCourse(id);

        model.addAttribute(course);
        model.addAttribute("tasks", tasks);
        model.addAttribute("notes", notes);

        return "courses/view";
    }

    @RequestMapping(value = "/{id}/edit", method = RequestMethod.GET)
    public String edit(@PathVariable("id") Long id, ModelMap model) {
        Course course = coursesApi.getCourse(id);

        EditCourseForm form = new EditCourseForm();
        form.setId(id);
        form.setTitle(course.getTitle());
        form.setComplexityId(course.getComplexity() != null ? course.getComplexity().getId() : null);
        form.setSummary(course.getSummary());
        form.setLectures(course.getLectures());
        form.setPractice(course.getPractice());

        model.addAttribute("form", form);
        model.addAttribute("department", course.getDepartment().getTitle());
        return "courses/edit";
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public String edit(@Valid @ModelAttribute("form") EditCourseForm form,
                       BindingResult result, final RedirectAttributes redirectAttributes) {

        if (result.hasErrors()) {
            return "courses/edit";
        }
        coursesApi.updateCourse(
                form.getId(), form.getTitle(), form.getComplexityId(), form.getSummary(),
                form.getLectures(), form.getPractice());

        redirectAttributes.addFlashAttribute(
                "message", MessageFormat.format("Course {0} was saved.", form.getTitle()));
        return "redirect:/courses/" + form.getId();
    }

    @RequestMapping(value = "/new", method = RequestMethod.GET)
    public String create(ModelMap model) {
        model.addAttribute("form", new ProposeCourseForm());
        return "courses/create";
    }

    @RequestMapping(value = "/new", method = RequestMethod.POST)
    public String create(@Valid @ModelAttribute("form") ProposeCourseForm form,
                         BindingResult result, final RedirectAttributes redirectAttributes) {

        if (result.hasErrors()) {
            return "courses/create";
        }
        try {
            coursesApi.startCourseProposal(form.getDepartmentId(), form.getTitle(), form.getInitiator(), form.getMotivation());
            redirectAttributes.addFlashAttribute(
                    "message", MessageFormat.format("Course {0} proposal process was started.", form.getTitle()));
            return "redirect:/courses";
        } catch (WorkflowException ex) {
            result.rejectValue("departmentId", "", ex.getMessage());
            return "courses/create";
        }
    }

    @ModelAttribute("complexityList")
    public List<CourseComplexity> addComplexities() {
        return coursesApi.getCourseComplexities();
    }

    @ModelAttribute("departmentList")
    public List<Department> addDepartments() {
        return orgStructureApi.getDepartmentsRunningCourses();
    }
}
