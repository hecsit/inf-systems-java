<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>

<html>
<head>
    <title><sitemesh:write property='title'/></title>
    <sitemesh:write property='head'/>

    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/main.css">
</head>

<s:url value="/courses" var="coursesListUrl" />
<s:url value="/tasks/by/it_faculty" var="itFacultyTasksUrl" />
<s:url value="/tasks/by/science_faculty" var="scienceFacultyTasksUrl" />
<s:url value="/tasks/by/it_dean_office" var="itTasksUrl" />
<s:url value="/tasks/by/science_dean_office" var="scienceTasksUrl" />
<s:url value="/tasks/by/provost" var="provostTasksUrl" />

<body>
    <h1>EDUCENTER</h1>
    <p>
        Courses: [<a href="${coursesListUrl}">List</a>]<br/>
        Tasks: [<a href="${itFacultyTasksUrl}">IT Faculty</a> |
        <a href="${itTasksUrl}">IT Dean Office</a>|
        <a href="${scienceFacultyTasksUrl}">Science Faculty</a> |
        <a href="${scienceTasksUrl}">Science Dean Office</a> |
        <a href="${provostTasksUrl}">Provost</a>]
    </p>
    <sitemesh:write property='body'/>
</body>
</html>