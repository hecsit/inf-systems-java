<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form"%>

<jsp:useBean id="courseList" scope="request" type="java.util.List"/>

<s:url value="/courses/new" var="createCourseUrl" />

<html>
<head>
    <title>Courses List</title>
</head>
<body>
<h3>Courses List [<a href="${createCourseUrl}">Add New</a> ]</h3>

<c:if test="${not empty message}">
    <p class="message">${message}</p>
</c:if>

<table>
    <tr><td>Code</td><td>Title</td><td>Complexity</td><td>Status</td></tr>
    <c:if test="${empty courseList}">
        <tr><td colspan="4">No Courses</td></tr>
    </c:if>
    <c:if test="${not empty courseList}">
        <c:forEach var="item" items="${courseList}">
            <s:url value="/courses/${item.id}" var="viewCourseUrl" />
            <tr>
                <td>${item.code}</td>
                <td><a href="${viewCourseUrl}">${item.title}</a></td>
                <td>${item.complexity.title}</td>
                <td>${item.status}</td>
            </tr>
        </c:forEach>
    </c:if>
</table>

</body>
</html>
