<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<s:url value="/courses/new" var="createCourseUrl" />

<html>
<head>
    <title>New Course</title>
</head>
<body>
<h3>New Course</h3>

<form:form method="post" modelAttribute="form" action="${createCourseUrl}">

    <label>Department:</label><form:select path="departmentId" items="${departmentList}" itemLabel="title" itemValue="id" />
    <form:errors path="departmentId" cssClass="error" />
    <br/>

    <label>Title:</label><form:input path="title" type="text" /><form:errors path="title" cssClass="error" />
    <br/>
    <label>Initiator:</label><form:input path="initiator" type="text" /><form:errors path="initiator" cssClass="error" />
    <br/>
    <label>Motivation:</label>
    <br/>
    <form:textarea path="motivation" rows="10" /><form:errors path="motivation" cssClass="error" />
    <br/>
    <button type="submit">Start Proposal Process</button>
</form:form>

</body>
</html>
