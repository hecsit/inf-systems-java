<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
<head>
    <title>${course.title}</title>
</head>
<body>

<s:url value="/courses/${course.id}/edit" var="editCourseUrl" />

<h3>
    Course: ${course.title}
    <c:if test="${course.status eq 'REVISION'}">
        [<a href="${editCourseUrl}">Edit</a>]
    </c:if>
</h3>

<c:if test="${not empty message}">
    <p class="message">${message}</p>
</c:if>

<p>Department: ${course.department.title}</p>
<p>Status: ${course.status}</p>
<p>Code: ${course.code}</p>
<p>Complexity: ${course.complexity.title}</p>
<p>Lectures: ${course.lectures}</p>
<p>Practice: ${course.practice}</p>
<p>Summary: ${course.summary}</p>

<c:if test="${not empty tasks}">
    <fieldset>
        <legend>Tasks</legend>
        <ul>
        <c:forEach var="task" items="${tasks}">
            <s:url value="/tasks/${task.id}" var="taskUrl" />
            <li>
                <a href="${taskUrl}">${task.description}</a>
            </li>
        </c:forEach>
        </ul>
    </fieldset>
</c:if>

<c:if test="${not empty notes}">
    <fieldset>
        <legend>Notes</legend>
        <ul>
            <c:forEach var="note" items="${notes}">
                <li>
                    [${note.author} @ <fmt:formatDate pattern="MM/dd/yyyy HH:mm" value="${note.timestamp}" />] ${note.text}
                </li>
            </c:forEach>
        </ul>
    </fieldset>
</c:if>

</body>
</html>
