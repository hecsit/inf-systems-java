<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<s:url value="/courses/update" var="updateCourseUrl" />

<html>
<head>
    <title>${form.title}</title>
</head>
<body>
<h3>Course: ${form.title}</h3>

<form:form method="post" modelAttribute="form" action="${updateCourseUrl}">
    <form:hidden path="id"/>
    <form:errors path="id" cssClass="error" /><br/>

    <label>Department:</label>${department}<br/>

    <label>Title:</label><form:input path="title" type="text" /><form:errors path="title" cssClass="error" />
    <label>Complexity:</label>
    <form:select path="complexityId">
        <form:option value="" label="(None)"/>
        <form:options items="${complexityList}" itemLabel="title" itemValue="id" />
    </form:select>
    <form:errors path="complexityId" cssClass="error" />
    <br/>
    <label>Lectures:</label><form:input path="lectures" type="text" /><form:errors path="lectures" cssClass="error" />
    <br/>
    <label>Practice:</label><form:input path="practice" type="text" /><form:errors path="practice" cssClass="error" />
    <br/>
    <label>Summary:</label>
    <br/>
    <form:textarea path="summary" rows="10" /><form:errors path="summary" cssClass="error" />
    <br/>
    <button type="submit">Save</button>
</form:form>

</body>
</html>
