<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
<head>
    <title>Tasks List</title>
</head>
<body>

<s:url value="/tasks/${task.id}/complete" var="completeTaskUrl" />
<s:url value="/courses/${task.processVariables.get('courseId')}" var="viewCourseUrl" />

<h3>Tasks List [${departmentTitle}]</h3>

<p>Task: <strong>${task.description}</strong></p>
<p>Department: ${departmentTitle}</p>
<p>Date: <fmt:formatDate pattern="MM/dd/yyyy" value="${task.createTime}" /></p>
<p>Due Date: <fmt:formatDate pattern="MM/dd/yyyy" value="${task.dueDate}" /></p>
<p>Course: ${task.processVariables.get('courseName')}</p>
<p>Initiator: ${task.processVariables.get('initiator')}</p>
<p>Motivation: ${task.processVariables.get('motivation')}</p>
<p><a href="${viewCourseUrl}">View Course</a></p>

<form action="${completeTaskUrl}" method="post">
    <c:forEach var="prop" items="${taskForm.formProperties}">
        <c:if test="${prop.type.name eq 'string'}">
            <label for="${prop.id}_form">${prop.name}</label>
            <input id="${prop.id}_form" name="${prop.id}" value="${prop.value}"/>
        </c:if>
        <c:if test="${prop.type.name eq 'enum'}">
            <label for="${prop.id}_form">${prop.name}</label>
            <select id="${prop.id}_form" name="${prop.id}">
                <c:forEach var="enumValue" items="${prop.type.getInformation('values')}">
                    <option value="${enumValue.key}">${enumValue.value}</option>
                </c:forEach>
            </select>
        </c:if>
        <br/>
    </c:forEach>

    <button type="submit">Complete Task</button>
</form>

</body>
</html>
