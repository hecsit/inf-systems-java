<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
<head>
    <title>Tasks List</title>
</head>
<body>
<h3>Tasks List [${departmentTitle}]</h3>

<table>
    <tr><td>Title</td><td>Date</td><td>Due Date</td></tr>
    <c:if test="${empty taskList}">
        <tr><td colspan="3">No Tasks</td></tr>
    </c:if>
    <c:if test="${not empty taskList}">
        <c:forEach var="item" items="${taskList}">
            <tr>
                <s:url value="/tasks/${item.id}" var="taskUrl" />

                <td><a href="${taskUrl}">${item.description}</a></td>
                <td><fmt:formatDate pattern="MM/dd/yyyy" value="${item.createTime}" /></td>
                <td><fmt:formatDate pattern="MM/dd/yyyy" value="${item.dueDate}" /></td>
            </tr>
        </c:forEach>
    </c:if>
</table>

</body>
</html>
