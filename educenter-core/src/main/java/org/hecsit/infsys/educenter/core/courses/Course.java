package org.hecsit.infsys.educenter.core.courses;

import org.hecsit.infsys.educenter.core.BaseEntity;
import org.hecsit.infsys.educenter.core.orgstructure.Department;

import javax.persistence.*;

@Entity
@Table(name = "COURSES")
public class Course extends BaseEntity {

    @Column(name = "code", length = 255)
    private String code;

    @Column(name = "title", length = 1024, nullable = false)
    private String title;

    @ManyToOne
    @JoinColumn(name = "complexity_id")
    private CourseComplexity complexity;

    @Column(name = "summary", length = 4000)
    private String summary;

    @Enumerated(EnumType.STRING)
    @Column(name = "status", length = 255, nullable = false)
    private CourseStatus status;

    @ManyToOne
    @JoinColumn(name = "department_id", nullable = false)
    private Department department;

    @Column(name = "lecture_hrs")
    private Integer lectures;

    @Column(name = "practice_hrs")
    private Integer practice;

    protected Course() {}

    public Course(Department department, String title, CourseStatus status) {
        this.department = department;
        this.title = title;
        this.status = status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public CourseComplexity getComplexity() {
        return complexity;
    }

    public void setComplexity(CourseComplexity complexity) {
        this.complexity = complexity;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public CourseStatus getStatus() {
        return status;
    }

    public void setStatus(CourseStatus status) {
        this.status = status;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public Integer getLectures() {
        return lectures;
    }

    public void setLectures(Integer lectures) {
        this.lectures = lectures;
    }

    public Integer getPractice() {
        return practice;
    }

    public void setPractice(Integer practice) {
        this.practice = practice;
    }
}
