package org.hecsit.infsys.educenter.core.orgstructure;

import org.hecsit.infsys.educenter.core.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "DEPARTMENTS")
public class Department extends BaseEntity {
    @Column(name = "code", length = 255)
    private String code;

    @Column(name = "title", length = 1024, nullable = false)
    private String title;

    @Column(name = "run_courses", nullable = false)
    private boolean runCourses;

    @Column(name = "course_proposal_process_key", length = 255)
    private String courseProposalProcessKey;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isRunCourses() {
        return runCourses;
    }

    public void setRunCourses(boolean runCourses) {
        this.runCourses = runCourses;
    }

    public String getCourseProposalProcessKey() {
        return courseProposalProcessKey;
    }

    public void setCourseProposalProcessKey(String courseProposalProcessKey) {
        this.courseProposalProcessKey = courseProposalProcessKey;
    }
}
