package org.hecsit.infsys.educenter.core.courses;

import org.hecsit.infsys.educenter.core.BaseEntity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "COURSE_NOTES")
public class CourseNote extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "course_id", nullable = false)
    private Course course;

    @Column(name = "timestamp", nullable = false)
    private Date timestamp;

    @Column(name = "author", length = 1024, nullable = false)
    private String author;

    @Column(name = "text", length = 4000, nullable = false)
    private String text;

    protected CourseNote() { }

    public CourseNote(Course course, Date timestamp, String author, String text) {
        this.course = course;
        this.timestamp = timestamp;
        this.author = author;
        this.text = text;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
