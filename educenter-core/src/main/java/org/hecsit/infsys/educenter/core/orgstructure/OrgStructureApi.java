package org.hecsit.infsys.educenter.core.orgstructure;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class OrgStructureApi {
    private final SessionFactory sessionFactory;

    @Autowired
    public OrgStructureApi(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public Department getDepartmentByCode(String code) {
        Session session = sessionFactory.getCurrentSession();
        return (Department) session.createQuery("from Department d where d.code = :code")
                .setString("code", code)
                .uniqueResult();
    }

    public List<Department> getDepartmentsRunningCourses() {
        Session session = sessionFactory.getCurrentSession();
        return (List<Department>) session.createQuery("from Department d where d.runCourses = true")
                .list();
    }
}
