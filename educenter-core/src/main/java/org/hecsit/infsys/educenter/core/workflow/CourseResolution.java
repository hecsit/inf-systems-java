package org.hecsit.infsys.educenter.core.workflow;

public enum CourseResolution {
    PROPOSED,
    AGREED,
    CONFIRMED,
    REJECTED,
    REVISION
}
