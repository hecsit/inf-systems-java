package org.hecsit.infsys.educenter.core.courses;

import org.activiti.engine.RuntimeService;
import org.hecsit.infsys.educenter.core.orgstructure.Department;
import org.hecsit.infsys.educenter.core.workflow.WorkflowException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class CoursesApi {

    private final SessionFactory sessionFactory;
    private final RuntimeService runtimeService;

    @Autowired
    public CoursesApi(SessionFactory sessionFactory, RuntimeService runtimeService) {
        this.sessionFactory = sessionFactory;
        this.runtimeService = runtimeService;
    }

    public Course getCourse(Long id) {
        Session session = sessionFactory.getCurrentSession();
        return session.byId(Course.class).load(id);
    }

    public List<CourseNote> getCourseNotes(long courseId) {
        Session session = sessionFactory.getCurrentSession();
        return (List<CourseNote>)session.createQuery("from CourseNote cn where cn.course.id = :courseId")
                .setLong("courseId", courseId)
                .list();
    }

    public List<Course> getCourses() {
        Session session = sessionFactory.getCurrentSession();
        List<Course> courses = (List<Course>)session.createQuery("from Course c left join fetch c.complexity")
                .list();
        return courses;
    }

    public List<CourseComplexity> getCourseComplexities() {
        Session session = sessionFactory.getCurrentSession();
        return (List<CourseComplexity>)session.createQuery("from CourseComplexity")
                .list();
    }

    public void updateCourse(Long id, String title, Long complexityId, String summary, Integer lectures, Integer practice) {
        Session session = sessionFactory.getCurrentSession();

        Course course = session.byId(Course.class).load(id);
        CourseComplexity complexity = session.byId(CourseComplexity.class).getReference(complexityId);

        course.setTitle(title);
        course.setComplexity(complexity);
        course.setSummary(summary);
        course.setLectures(lectures);
        course.setPractice(practice);
    }

    public void startCourseProposal(long departmentId, String title, String initiator, String motivation) {
        Session session = sessionFactory.getCurrentSession();
        Department department = session.byId(Department.class).load(departmentId);
        if (department.getCourseProposalProcessKey() == null) {
            throw new WorkflowException("Cannot start a course proposal process for '" + department.getTitle() + "'");
        }

        Course course = new Course(department, title, CourseStatus.REVISION);
        session.save(course);

        CourseNote note = new CourseNote(course, new java.util.Date(), initiator, motivation);
        session.save(note);

        Map<String, Object> variables = new HashMap<>();
        variables.put("courseId", course.getId());
        variables.put("courseName", course.getTitle());
        variables.put("initiator", initiator);
        variables.put("motivation", motivation);
        variables.put("dueDate", Date.from(
                LocalDate.now().plus(1, ChronoUnit.WEEKS).atStartOfDay(ZoneId.systemDefault()).toInstant()));

        runtimeService.startProcessInstanceByKey(department.getCourseProposalProcessKey(), variables);
    }
}
