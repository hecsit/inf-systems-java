package org.hecsit.infsys.educenter.core.lib.hibernate;

import java.sql.Types;

public class SQLServerDialect extends org.hibernate.dialect.SQLServer2008Dialect {
    public SQLServerDialect() {
        super();
        this.registerColumnType(Types.LONGVARCHAR, "nvarchar(MAX)");
        this.registerColumnType(Types.VARCHAR, "nvarchar(MAX)");
        this.registerColumnType(Types.VARCHAR, 8000L, "nvarchar($l)");
        registerColumnType(Types.CLOB, "nvarchar(max)");
    }
}
