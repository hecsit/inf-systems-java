package org.hecsit.infsys.educenter.core.tasks;

import org.activiti.engine.FormService;
import org.activiti.engine.TaskService;
import org.activiti.engine.form.TaskFormData;
import org.activiti.engine.task.IdentityLink;
import org.activiti.engine.task.IdentityLinkType;
import org.activiti.engine.task.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@Transactional
public class TasksApi {

    private final TaskService taskService;
    private final FormService formService;

    @Autowired
    public TasksApi(TaskService taskService, FormService formService) {
        this.taskService = taskService;
        this.formService = formService;
    }

    public List<Task> getTasksByCourse(Long courseId) {
        List<Task> tasks = taskService.createTaskQuery()
                .includeProcessVariables()
                .processVariableValueEquals("courseId", courseId)
                .orderByTaskCreateTime()
                .desc()
                .list();
        return tasks;
    }

    public List<Task> getTasksByCandidateGroup(String candidateGroup) {
        List<Task> tasks = taskService.createTaskQuery()
                .taskCandidateGroup(candidateGroup)
                .list();
        return tasks;
    }

    public Task getTask(String taskId) {
        Task task = taskService.createTaskQuery()
                .taskId(taskId)
                .includeProcessVariables()
                .singleResult();
        return task;
    }

    public TaskFormData getTaskForm(String taskId) {
        return formService.getTaskFormData(taskId);
    }

    public String getTaskCandidateGroup(String taskId) {
        Optional<IdentityLink> candidate = taskService.getIdentityLinksForTask(taskId)
                .stream()
                .filter(x -> x.getType().equals(IdentityLinkType.CANDIDATE) && x.getUserId() == null && x.getGroupId() != null)
                .findFirst();

        return candidate.isPresent()
                ? candidate.get().getGroupId()
                : null;
    }

    public void complete(String taskId, Map<String, String> form) {
        formService.submitTaskFormData(taskId, form);
    }
}
