package org.hecsit.infsys.educenter.core.courses;

public enum CourseStatus {
    REVISION,
    CONFIRMATION,
    REJECTED,
    CONFIRMED
}
