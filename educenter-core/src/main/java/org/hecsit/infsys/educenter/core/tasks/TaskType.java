package org.hecsit.infsys.educenter.core.tasks;

import org.activiti.engine.task.Task;

public enum TaskType {
    COURSE_DRAFT("DRAFT"),
    COURSE_REVISION("REVISION"),
    COURSE_RECONCILIATION("RECONCILIATION"),
    COURSE_CONFIRMATION("CONFIRMATION"),
    COURSE_REGISTRATION("REGISTRATION");

    private final String key;

    TaskType(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    public boolean is(Task task) {
        return task != null && key.equals(task.getName());
    }
}
