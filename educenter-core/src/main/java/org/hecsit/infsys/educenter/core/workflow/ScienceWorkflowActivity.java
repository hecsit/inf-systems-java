package org.hecsit.infsys.educenter.core.workflow;

import org.activiti.engine.delegate.DelegateExecution;
import org.hecsit.infsys.educenter.core.courses.Course;
import org.hecsit.infsys.educenter.core.courses.CourseNote;
import org.hecsit.infsys.educenter.core.courses.CourseStatus;
import org.hecsit.infsys.educenter.core.orgstructure.Department;
import org.hecsit.infsys.educenter.core.orgstructure.OrgStructureApi;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class ScienceWorkflowActivity {

    private final SessionFactory sessionFactory;
    private final OrgStructureApi orgStructureApi;

    @Autowired
    public ScienceWorkflowActivity(SessionFactory sessionFactory, OrgStructureApi orgStructureApi) {
        this.sessionFactory = sessionFactory;
        this.orgStructureApi = orgStructureApi;
    }

    public void propose(DelegateExecution execution, Long courseId, String departmentCode, String resolution, String comment) {
        CourseResolution courseResolution = CourseResolution.valueOf(resolution);
        Session session = sessionFactory.getCurrentSession();
        Course course = session.byId(Course.class).load(courseId);
        Department department = orgStructureApi.getDepartmentByCode(departmentCode);
        String noteText = "";

        if (CourseResolution.PROPOSED.equals(courseResolution)) {
            course.setStatus(CourseStatus.CONFIRMATION);
            noteText = "Course is proposed: " + comment;
        } else {
            course.setStatus(CourseStatus.REJECTED);
            noteText = "Course is rejected: " + comment;
        }

        CourseNote note = new CourseNote(course, new Date(), department.getTitle(), noteText);
        session.save(note);
    }

    public void reconciliation(DelegateExecution execution, Long courseId, String departmentCode, String resolution, String comment) {
        CourseResolution courseResolution = CourseResolution.valueOf(resolution);
        Session session = sessionFactory.getCurrentSession();
        Course course = session.byId(Course.class).getReference(courseId);
        Department department = orgStructureApi.getDepartmentByCode(departmentCode);
        String noteText = "";

        if (CourseResolution.AGREED.equals(courseResolution)) {
            noteText = "Course is agreed: " + comment;
        } else if (CourseResolution.REVISION.equals(courseResolution)) {
            noteText = "Course is sent to revision: " + comment;
        } else {
            noteText = "Course is rejected: " + comment;
        }

        CourseNote note = new CourseNote(course, new Date(), department.getTitle(), noteText);
        session.save(note);
    }

    public void confirmation(DelegateExecution execution, Long courseId, String departmentCode, String resolution, String comment) {
        CourseResolution courseResolution = CourseResolution.valueOf(resolution);
        Session session = sessionFactory.getCurrentSession();
        Course course = session.byId(Course.class).getReference(courseId);
        Department department = orgStructureApi.getDepartmentByCode(departmentCode);
        String noteText = "";

        if (CourseResolution.CONFIRMED.equals(courseResolution)) {
            noteText = "Course is confirmed: " + comment;
        } else {
            noteText = "Course is rejected: " + comment;
        }

        CourseNote note = new CourseNote(course, new Date(), department.getTitle(), noteText);
        session.save(note);
    }

    public void rejected(DelegateExecution execution, Long courseId) {
        Session session = sessionFactory.getCurrentSession();
        Course course = session.byId(Course.class).load(courseId);
        course.setStatus(CourseStatus.REJECTED);
    }

    public void confirmed(DelegateExecution execution, Long courseId) {
        String courseCode = (String)execution.getVariable("code");

        Session session = sessionFactory.getCurrentSession();
        Course course = session.byId(Course.class).load(courseId);
        course.setStatus(CourseStatus.CONFIRMED);
        course.setCode(courseCode);
    }

    public void revision(DelegateExecution execution, Long courseId) {
        Session session = sessionFactory.getCurrentSession();
        Course course = session.byId(Course.class).load(courseId);
        course.setStatus(CourseStatus.REVISION);
    }
}
