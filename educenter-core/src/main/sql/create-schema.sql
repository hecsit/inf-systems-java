create table [dbo].[COURSE_COMPLEXITIES] (
  [id] bigint identity (1, 1) not null,
  [title] nvarchar(1024) not null,

  constraint [PK_COURSE_COMPLEXITY] primary key clustered ([id] asc)
)
go

create table [dbo].[DEPARTMENTS] (
  [id] bigint identity (1, 1) not null,
  [code] nvarchar(255) null,
  [title] nvarchar(1024) not null,
  [run_courses] bit not null,
  [course_proposal_process_key] nvarchar(255) null,

  constraint [PK_DEPARTMENT] primary key clustered ([id] asc)
)
go

set identity_insert [dbo].[DEPARTMENTS] on
insert into [dbo].[DEPARTMENTS] ([id], [code], [title], [run_courses], [course_proposal_process_key])
  values (1, 'it_faculty', 'IT Faculty', 1, null);
insert into [dbo].[DEPARTMENTS] ([id], [code], [title], [run_courses], [course_proposal_process_key])
  values (2, 'science_faculty', 'Science Faculty', 1, 'science_course_confirmation');
insert into [dbo].[DEPARTMENTS] ([id], [code], [title], [run_courses]) values (3, 'it_dean_office', 'IT Dean Office', 0);
insert into [dbo].[DEPARTMENTS] ([id], [code], [title], [run_courses]) values (4, 'science_dean_office', 'Science Dean Office', 0);
insert into [dbo].[DEPARTMENTS] ([id], [code], [title], [run_courses]) values (5, 'provost', 'Provost', 0);
set identity_insert [dbo].[DEPARTMENTS] off

create table [dbo].[COURSES] (
  [id] bigint identity (1, 1) not null,
  [code] nvarchar(255) null,
  [title] nvarchar(1024) not null,
  [complexity_id] bigint null,
  [summary] nvarchar(4000) null,
  [status] nvarchar(255) not null,
  [department_id] bigint not null,
  [lecture_hrs] int null,
  [practice_hrs] int null,

  constraint [PK_COURSE] primary key clustered ([id] asc)
)
go

alter table [dbo].[COURSES] add constraint [FK_COURSE_COURSE_COMPLEXITY]
  foreign key ([complexity_id]) references [dbo].[COURSE_COMPLEXITIES] ([id])
go

alter table [dbo].[COURSES] add constraint [FK_COURSE_DEPARTMENT]
  foreign key ([department_id]) references [dbo].[DEPARTMENTS] ([id])
go

create table [dbo].[COURSE_NOTES] (
  [id] bigint identity (1, 1) not null,
  [course_id] bigint not null,
  [timestamp] datetime not null,
  [author] nvarchar(1024) not null,
  [text] nvarchar(4000) not null,

  constraint [PK_COURSE_NOTE] primary key clustered ([id] asc)
)
go

alter table [dbo].[COURSE_NOTES] add constraint [FK_COURSE_NOTE_COURSE]
  foreign key ([course_id]) references [dbo].[COURSES] ([id])
go


