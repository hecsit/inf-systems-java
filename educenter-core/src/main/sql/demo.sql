set identity_insert [dbo].[COURSE_COMPLEXITIES] on

insert into [dbo].[COURSE_COMPLEXITIES] ([id], [title])
  values (1, 'Basic');

insert into [dbo].[COURSE_COMPLEXITIES] ([id], [title])
  values (2, 'Medium');

insert into [dbo].[COURSE_COMPLEXITIES] ([id], [title])
  values (3, 'Complex');

insert into [dbo].[COURSE_COMPLEXITIES] ([id], [title])
  values (4, 'Professional');

set identity_insert [dbo].[COURSE_COMPLEXITIES] OFF
go

set identity_insert [dbo].[COURSES] on

insert into [dbo].[COURSES] ([id], [code], [title], [complexity_id], [summary], [status], [department_id], [lecture_hrs], [practice_hrs])
  values (1, 'IT.BASE.001', 'Algorithms', 2, 'Introduction into Algorithms.', 'CONFIRMED', 1, 54, 108);
insert into [dbo].[COURSES] ([id], [code], [title], [complexity_id], [summary], [status], [department_id], [lecture_hrs], [practice_hrs])
  values (2, 'IT.EL.007', 'Object-Oriented Programming', 2, 'Principles and Patterns of Object-Oriented software design.', 'CONFIRMED', 1, 54, 81);

set identity_insert [dbo].[COURSES] off